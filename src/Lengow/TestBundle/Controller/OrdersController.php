<?php

namespace Lengow\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lengow\TestBundle\Entity\Orders;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use APY\DataGridBundle\Grid\Source\Vector;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

class OrdersController extends Controller
{

    public function indexAction()
    {
        $orders = $this->container->get('lengow_test');
        
        return $this->render('LengowTestBundle:Orders:index.html.twig', array('orders' => $orders->getFromXML()));
    }
    public function orderAction()
    {
        $orders = $this->container->get('lengow_test');
        $xml = $orders->getFromXML();
        $orderEntities = $orders->parseXML($xml);
        // once we have all the orders, we can save them in db
        $em = $this->getDoctrine()->getManager();
        foreach ($orderEntities as $order){
            // if no similar order is found, we can save it to db
            if(!$this->Exists($order))
                $em->persist($order);
        }
        $em->flush();
        return $this->render('LengowTestBundle:Orders:orders.html.twig', array('orders' => $orderEntities));
    }
    public function gridAction(){
        //following the doc : https://github.com/APY/APYDataGridBundle
        $em = $this->getDoctrine()->getManager();
        $or =  $em->getRepository('LengowTestBundle:Orders')->findAll();
        $source = new Vector($or);
        $grid = $this->get('grid');
        $grid->setSource($source);
        return $grid->getGridResponse('LengowTestBundle:Orders:grid.html.twig');
    }

    public function createAction(Request $request){

        $order = new Orders();
        $form = $this->createFormBuilder($order)
            ->add('marketplace','text')
            ->add('orderId','text')
            ->add('orderPurchaseDate','date')
            ->add('orderAmount','number')
            ->add('save', 'submit', array('label' => 'Create order'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            return $this->redirectToRoute('orders_grid');
        }
        return $this->render('LengowTestBundle:Orders:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    public function apiGetAllAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        //we could use :
        //$or =  $em->getRepository('LengowTestBundle:Orders')->findAll();
        //but findAll returns a collection, but we need an array, so we should build
        // a custom query :

        $query = $em->createQuery("select o from LengowTestBundle:Orders o");
        $or = $query->getArrayResult();

        if($request->query->get('format') == "yml")
           return new Response(Yaml::dump($or));
        return new JsonResponse($or);
    }
    public function apiGetOneAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("select o from LengowTestBundle:Orders o where o.id = :id")
            ->setParameter('id',$id);
        $or = $query->getArrayResult();
        if($request->query->get('format') == "yml")
            return new Response(Yaml::dump($or));
        return new JsonResponse($or);
    }

    private function Exists(Orders $order){
        $em = $this->getDoctrine()->getManager();
        // assuming that orderId is uniq :
        $or =  $em->getRepository('LengowTestBundle:Orders')->findOneByOrderId($order->getOrderId());
        //so we return true if an order is found
        if ($or)
            return true;
        return false;
    }
}
