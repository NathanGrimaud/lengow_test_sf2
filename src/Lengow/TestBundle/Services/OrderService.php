<?php
namespace Lengow\TestBundle\Services;

use Symfony\Component\Serializer\Serializer;
use Lengow\TestBundle\Entity\Orders;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OrderService
{

    private $ordersUrl;
    private $logger;
    public function __construct($url, $logger){
        //the parameter url is going to be filled with the parameter url_order in app/config/parameteryml
        $this->ordersUrl = $url;
        // the logger used is monolog, the base logger of symfony
        $this->logger = $logger;

        $encoder = array(new XmlEncoder());
        $normalizer = array(new ObjectNormalizer());
        $this->serializer = new Serializer($normalizer,$encoder);
    }
    public function getFromXML(){

        // the logger writes on symfony logs (app/logs/dev>log)
        $this->logger->info("going to download the file from ".$this->ordersUrl);

        try {
          $xmlStream = file_get_contents($this->ordersUrl);
            $this->logger->info("file sucessfully downloaded");
        } catch (\Exception $e) {
          $this->logger->info("an error have occured : ".$e->getMessage());
            throw $e;
        }
        return $xmlStream;
    }

    public function parseXML($xmlOrders){
            $realXML = simplexml_load_string($xmlOrders);
            $allOrders = [];
            foreach ($realXML->orders[0] as $key => $order) {
               $or = new Orders();
                $or->setMarketplace($order->marketplace);
                $or->setOrderAmount($order->order_amount);
                $or->setOrderId($order->order_id);

                $date = $order->order_purchase_date;
                $or->setOrderPurchaseDate(new \DateTime($date));

                array_push($allOrders, $or);
            }
            return $allOrders;
    }
    private function checkXML($xmlOrders){
        if(isset($xmlOrders->orders))
            return true;
        return false;
    }
}
